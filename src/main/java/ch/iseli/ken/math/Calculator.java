package ch.iseli.ken.math;

import java.math.BigDecimal;

public class Calculator {

	/**
	 * @return the sum of the given summands.
	 */
	public BigDecimal sum(BigDecimal summand1, BigDecimal summand2) {
		return summand1.add(summand2);
	}


}
