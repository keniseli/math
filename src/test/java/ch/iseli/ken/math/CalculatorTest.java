package ch.iseli.ken.math;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ch.iseli.ken.math.Calculator;


public class CalculatorTest {
	private Calculator calculator;
	
	@Before
	public void setUp() {
		calculator = new Calculator();
	}

	@Test
	public void testSumPositive() {
		BigDecimal sum = calculator.sum(BigDecimal.TEN, BigDecimal.ONE);
		Assert.assertEquals(BigDecimal.valueOf(11), sum);
	}
	
	@Test
	public void testSumNegative() {
		BigDecimal sum = calculator.sum(BigDecimal.ONE, new BigDecimal(-5));
		Assert.assertEquals(BigDecimal.valueOf(-4), sum);
		Assert.fail();
	}
	
}
